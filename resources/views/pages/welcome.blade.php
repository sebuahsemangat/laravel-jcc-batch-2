<!DOCTYPE html>
<html>
<head>
<title>Welcome | {{$fname}} {{$lname}}</title>
</head>
<body>

<h1>Welcome {{$fname}} {{$lname}}</h1>
<h3>Terima kasih telah bergabung dengan Sanberbook. Social Media kita bersama!</h3>
<p>Berikut ini adalah data diri kamu:</p>
<table>
	<tr>
		<td>Nama Lengkap</td>
		<td>:<td>
		<td>{{$fname}} {{$lname}}<td>
	</tr>
	<tr>
		<td>Gender</td>
		<td>:<td>
		<td>{{$gender}}<td>
	</tr>
	<tr>
		<td>Nationality</td>
		<td>:<td>
		<td>{{$nationality}}<td>
	</tr>
	<tr>
		<td>Bio</td>
		<td>:<td>
		<td>{{$bio}}<td>
	</tr>
	<tr>
		<td></td>
		<td><td>
		<td><a href="/">Kembali ke Home</a><td>
	</tr>
</table>
</body>
</html>