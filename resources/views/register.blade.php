<!DOCTYPE html>
<html>
<head>
<title>Form | Apep Wahyudin</title>
</head>
<body>
<h3>Buat Account Baru</h3>
<h5>Sign Up Form</h5>
	<form action="/welcome" method="post">
@csrf
<label>First Name</label><br><br>
<input name="first_name" type="text"><br><br>
<label>Last Name</label><br><br>
<input name="last_name" type="text"><br><br>
<label>Gender</label><br><br>
<input type="radio" name="gender" value="male">Male<br>
<input type="radio" name="gender" value="female">Female<br><br>
<label>Nationality</label><br><br>
<select name="nationality">
	<option value="id">Indonesia</option>
	<option value="en">Inggris</option>
	<option value="us">Amerika</option>
</select>
<br><br>
<label>Language Spoken</label>
<br><br>
<input type="checkbox" name="id" value="id">Bahasa Indonesia<br>
<input type="checkbox" name="en" value="en">English<br>
<input type="checkbox" name="oth" value="oth">Other<br>
<br>
<label>Bio</label><br><br>
<textarea name="bio"></textarea>
<br><br>
<input type="submit" name="submit" value="Sign Up">
	</form>
</body>
</html>