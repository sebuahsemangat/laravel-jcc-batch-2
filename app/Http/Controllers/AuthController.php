<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist (){
    return view ('register');
}

    public function sendData (Request $request){
        // untuk menampilkan semua inputan dd($request->all());

        //definisi variabel dulu
        $fname = $request['first_name'];
        $lname = $request['last_name'];
        $gender = $request['gender'];
        $nationality = $request ['nationality'];
        $id = $request['id'];
        $en = $request['en'];
        $oth = $request['oth'];
        $bio = $request['bio'];

        //masuk ke halaman welcome dengan membawa data
        return view ('pages.welcome', compact('fname','lname','gender','nationality','id','en','oth','bio'));
    }
}
